"use strict"

# Controllers
app = angular.module("myApp.controllers", [])


app.controller('MainCtrl',($rootScope,AuthenticationService)->
  $rootScope.pageTitle        = '';
  $rootScope.pageDescription  = '';
  $rootScope.pageKeyword      = '';

  $rootScope.alerts           = [];

  $rootScope.closeAlert       = (index) ->
    $rootScope.alerts.splice(index, 1);

  $rootScope.emptyAlert       = ->
    $rootScope.alerts         = [];


  # Check is admin
  $rootScope.isAdmin    = ->
    AuthenticationService.level    == 'admin';

  # Check is admin
  $rootScope.isMember    = ->
    AuthenticationService.level    == 'member';

  # Check is guest
  $rootScope.isGuest    = ->
    AuthenticationService.level    == 'guest';

  # Check login
  $rootScope.isLogin    = ->
    AuthenticationService.isLogged;

)