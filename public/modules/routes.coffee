"use strict"

# Routes
app   = angular.module("myApp.routes", [])

app.config(['$stateProvider','$urlRouterProvider','$locationProvider',($stateProvider, $urlRouterProvider ,$locationProvider)->

  $urlRouterProvider.otherwise("/home");

  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');
]);