"use strict"

# Routes
app   = angular.module("home.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider
  .state('home', {
      url         : "/home",
      templateUrl : "modules/home/views/index.html",
      controller  : 'HomeCtrl'
    })
]);