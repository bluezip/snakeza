'use strict';

app = angular.module('myApp.home', [
  'home.controllers',
  'home.routes',
]);