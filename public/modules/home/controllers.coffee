"use strict"

# Controllers
app = angular.module("home.controllers", []);

# Home controller
app.controller("HomeCtrl",($scope,$filter,$rootScope)->
  $rootScope.pageTitle      = $filter('translate')('Home title');
);