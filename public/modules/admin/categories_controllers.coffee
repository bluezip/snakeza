"use strict"

# Controllers
app = angular.module("admin.categoriesControllers", []);

app.controller('AdminCategoriesCtrl',($scope,$filter,$rootScope,$state,AdminCategoriesApi,$modal)->
  $rootScope.pageTitle  = $filter('translate')('Categories - Admin');

  $scope.page = $state.params.page;

  AdminCategoriesApi.query({},(data)->
    if $state.params.page?
      data  =  _.where(data,{main : false });
      data  =  _.filter(data,(val)->
        url = $state.params.page
        rex = new RegExp('^\/'+url,'i');
        val.url.match(rex);
      );
    else
      data        =  _.where(data,{main : true})

#    data          =  _.sortBy(data,(val)-> return new Date(val.update_at).getTime() );
#    $scope.list   = data.reverse();

    data          =  _.sortBy(data,(val)->
      return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig,'').charCodeAt(0));
    );
    $scope.list   = data;
  );

  $scope.delete = (data) ->
    if(confirm($filter('translate')('Confirm delete?')))
      AdminCategoriesApi.delete({id : data._id}
      ,(data)->
        $state.go('admin.categories',{}, {reload: true});
      ,(err)->

      );
);



app.controller('AdminCategoriesSaveCtrl',($scope,$filter,AdminCategoriesApi,$state,$rootScope)->

  if $state.params.id?
    AdminCategoriesApi.get({id : $state.params.id}
    ,(data)->
      $scope.data = data;
    ,(err)->

    );
    $scope.update   = true;

  AdminCategoriesApi.query({},(data)->
    data          =  _.where(data,{main : true});
    data          = _.sortBy(data,(val)-> return new Date(val.update_at).getTime() );
    $scope.list   = data.reverse();
  );

  $scope.selectItem = (item)->
    url = $scope.data?.url?.split('/')[1];
    rex = new RegExp('^\/'+url,'i');
    if item.match(rex)
      return true;
    else
      return false;


  goto    = (data)->
    if data.main == true
      $state.go('admin.categories');
    else
      url = $scope.data.url?.split('/')[1];
      $state.go('admin.categories.sub',{page : url});

  error   = ->
    $rootScope.alerts.push({
      msg   : $filter('translate')('Please recheck data?'),
      type  : 'danger'
    });

  # save
  $scope.save = (vaild) ->
    if vaild
      if $state.params.id?
        AdminCategoriesApi.update({
            data  : $scope.data
            id    : $state.params.id
          }
        ,(data)-> # success
          goto($scope.data);
        ,(err)-> # error
          error();
        );
      else
        AdminCategoriesApi.save({data : $scope.data }
        ,(data)->
          goto($scope.data);
        ,(err)->
          error();
        );
    # end if
);