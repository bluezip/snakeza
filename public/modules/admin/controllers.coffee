"use strict"

# Controllers
app = angular.module("admin.controllers", []);

# For directives admin-menu
app.controller('AdminMenuCtrl',($scope,$location)->

  $scope.isActive = (route) ->
    route.split('/')[2] == $location.$$path.split('/')[2]


);

app.controller('AdminHomeCtrl',($scope)->

);