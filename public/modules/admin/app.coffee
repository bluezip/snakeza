'use strict';

app = angular.module('myApp.admin', [
  'admin.routes',
  'admin.controllers',
  'admin.directives',
  'admin.services',

  'admin.categoriesControllers',
  'admin.userControllers',
]);