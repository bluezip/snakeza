"use strict"

# Service
app   = angular.module("admin.services", ['ngResource']);

app.factory('AdminCategoriesApi',($resource,apiRoot)->
  $resource(apiRoot+'/admin/category/:id',{
    id : '@id'
  },{
    query : {
      url     : apiRoot+'/category',
      method  : "GET",
      isArray : true,
      cache   : false,
    },
    save : {
      method : "POST",
    },
    update : {
      method : "PUT"
    },
    delete : {
      method : "DELETE"
    }
  });
);


app.factory('AdminUserApi',($resource,apiRoot)->
  $resource(apiRoot+'/admin/user/:id',{
    id : '@id'
  },{
    query : {
      url     : apiRoot+'/admin/user',
      method  : "GET",
      isArray : true
    },
    save : {
      method : "POST",
    },
    update : {
      method : "PUT"
    },
    delete : {
      method : "DELETE"
    }
  });
);