"use strict"

# Directives
app   = angular.module("admin.directives", [])

app.directive('adminMenu',->
  {
    templateUrl : 'modules/admin/views/html/_left-menu.html',
    controller : 'AdminMenuCtrl'
  }
);