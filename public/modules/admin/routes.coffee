"use strict"

# Routes
app   = angular.module("admin.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider

  .state('admin', {
      url         : "/admin",
      templateUrl : "modules/admin/views/home/index.html",
      controller  : 'AdminHomeCtrl',
      access      : { requiredLogin: true , level : 'admin' }
    })

  .state('admin.categories', {
      url         : "/categories",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/index.html",
          controller  : 'AdminCategoriesCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin' }
    })

  .state('admin.categories.create', {
      url         : "/create",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/_form.html",
          controller  : 'AdminCategoriesSaveCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin'}
    })

  .state('admin.categories.update', {
      url         : "/update/:id",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/_form.html",
          controller  : 'AdminCategoriesSaveCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin'}
    })

  .state('admin.categories.sub', {
      url         : "/sub/:page",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/index.html",
          controller  : 'AdminCategoriesCtrl',
          access      : { requiredLogin: true , level : 'admin'}
        }
      }
    })

  .state('admin.user',{
      url     : "/user",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/index.html",
          controller  : "AdminUserCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })

  .state('admin.user.create',{
      url     : "/create",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/create.html",
          controller  : "AdminUserCreateCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })

  .state('admin.user.update',{
      url     : "/update/:id",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/update.html",
          controller  : "AdminUserUpdateCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })
]);