"use strict"

# Directives
app   = angular.module("myApp.directives", [])


app.directive('appVersion',->
  {
    link : (scope,element,attr)->
      version = jQuery('config').attr('version');
      element.text("website.com V. #{version}");
  }
);

# Rename router for html5
app.directive('a',['$location',($location)->
  {
  restrict  : 'E',
  scope : {},
  link : (scope,element,attr)->
    if $location.$$html5 == false
      rename = (e = null)->
        if e? && (jQuery(e).attr('href')?.match(/[^http|#]/ig))
          href  = jQuery(e).attr('href')
          href  = '/'+href if !href.match(/^\//ig)
          jQuery(e).attr('href','#!'+href);

      # When clink
      element.bind('click',->
        rename(@);
      );
  }
])


# Load session form browser
app.directive('config',['$rootScope','$window','AuthenticationService',($rootScope,$window,AuthenticationService)->
  {
    restrict  : 'E',
    link : (scope,element,attr)->
      AuthenticationService.isLogged  = true if $window.sessionStorage.auth
      AuthenticationService.level     = $window.sessionStorage.level if $window.sessionStorage.level?
  }
])


# Adsense 728
app.directive('ads728', ->
  {
    restrict: 'A',
    templateUrl: 'partials/ads/ads-728.html',
    controller:->
      (adsbygoogle = window.adsbygoogle || []).push({});
  }
);

# Adsense 300
app.directive('ads300', ->
  {
  restrict: 'A',
  templateUrl: 'partials/ads/ads-300.html',
  controller:->
    (adsbygoogle = window.adsbygoogle || []).push({});
  }
);

# Adsense 160
app.directive('ads160', ->
  {
  restrict: 'A',
  templateUrl: 'partials/ads/ads-160.html',
  controller:->
    (adsbygoogle = window.adsbygoogle || []).push({});
  }
);