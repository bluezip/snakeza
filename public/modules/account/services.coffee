"use strict"

# Service
app   = angular.module("account.services", ['ngResource']);

# Auth url
app.factory("AccountAuthApi",($resource,apiRoot)->
  $resource(apiRoot+'/auth',{},{
    login : {
      url : apiRoot+'/auth'
      method : 'POST',
      params : {}
    }
  });
);


# User api url
app.factory("AccountUsersApi",($resource,apiRoot)->

  $resource(apiRoot+'/user',{}, {

  # Create account
    register  :{
      method  : 'POST'
    },

  # Check unique email
    emailUnique : {
      url     : apiRoot+'/user/email',
      method  : 'GET',
      params  : {
        email : '@email',
      }
    },

  # Check unique username
    usernameUnique : {
      url     : apiRoot+'/user/username',
      method  : 'GET',
      params  : {
        username  : '@username',
      }
    },
  });
);

###
  Profile api url
###
app.factory('AccountProfileApi',($resource,apiRoot)->
  $resource(apiRoot+'/profile',{}, {
    get : {
      method  : "GET",
      url     : apiRoot+'/profile/info',
      cache   : false,
    },
    update : {
      method  : "PUT",
      url     : apiRoot+'/profile/info',
    },
    password : {
      method  : "PUT",
      url     : apiRoot+'/profile/password',
    }
  });
)
