"use strict"

# Routes
app   = angular.module("account.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider
  .state('register',{
    url         : '/register',
    templateUrl : 'modules/account/views/register.html',
    controller  : ($rootScope,$filter)->
      $rootScope.pageTitle       = $filter('translate')('Register');

  })

  .state('login',{
    url         : '/login',
    templateUrl : 'modules/account/views/register.html',
    controller  : ($scope,$rootScope,$filter)->
      $rootScope.pageTitle      = $filter('translate')('Login');
    })

  .state('account',{
    url : '/account',
    views : {
      ''  : {
        templateUrl   : 'modules/account/views/index.html',
      },
      'edit@account'  : {
        templateUrl   : 'modules/account/views/account.html',
        controller    : 'AccountCtrl'
      }
    },
    access: { requiredLogin: true }
  })

  .state('account.edit',{
    url : '/:page',
    views : {
      'edit@account'  : {
        templateUrl   : 'modules/account/views/account.html',
        controller    : 'AccountCtrl'
      }
    },
    access: { requiredLogin: true }
  })
]);