'use strict';

app = angular.module('myApp.account', [
  'account.controllers',
  'account.routes',
  'account.directives',
  'account.services',
]);