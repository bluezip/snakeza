"use strict"

# Controllers
app = angular.module("account.controllers", []);


###
  For directives (loginForm)
###
app.controller('LoginFormCtrl',($scope, AccountAuthApi, $window, $rootScope,$filter,AuthenticationService,$location)->
  $scope.loginForm = (valid)->
    if(valid)
      AccountAuthApi.login({
          email     : $scope.email
          password  : $scope.password
        }, (data)-> # success
          AuthenticationService.isLogged  = true;
          $window.sessionStorage.level    = AuthenticationService.level     = data.level;
          $window.sessionStorage.auth     = data.auth;
          $scope.email  = $scope.password   = '' # set null data
          $rootScope.alerts.push({
            msg     : $filter('translate')('Login already'),
            type    : 'success'
          });
          $location.path("/account");
      ,(err) -> # error
        AuthenticationService.isLogged  = false;
        delete $window.sessionStorage.level;
        delete $window.sessionStorage.auth;
        $rootScope.alerts.push({
          msg     : $filter('translate')('Email or password is incorrect.'),
          type    : 'danger'
        });
      )# end Auth.login()
    else
      $rootScope.alerts.push({
        msg     : $filter("translate")("Login please recheck data ?"),
        type    : 'danger'
      });
)


###
 Register form
###

app.controller('RegisterFormCtrl',($scope, AccountAuthApi, AccountUsersApi, $window, $rootScope,$filter,AuthenticationService,$location)->

  # Auto login
  autoLogin   = ->
    AccountAuthApi.login({
        email     : $scope.email
        password  : $scope.password
      }
    ,(data)-> # success
      AuthenticationService.isLogged  = true;
      $window.sessionStorage.auth     = data.auth;
      $location.path("/account");
    ,(err)-> # error
      AuthenticationService.isLogged  = false;
      delete $window.sessionStorage.level;
      delete $window.sessionStorage.auth;
    )# end Auth.login()


  $scope.registerForm = (valid) ->
    if(valid)
      AccountUsersApi.register({
          email     : $scope.email,
          password  : $scope.password,
          username  : $scope.username
        }
      ,(data)-> # success
        autoLogin();
        $scope.email  = $scope.rePassword  = $scope.password   = '' # set null data
        $rootScope.alerts.push({
          msg     : $filter('translate')('Already Registered'),
          type    : 'success'
        });

      , (err)-> # error
        $rootScope.alerts.push({
          msg     : err.message,
          type    : 'warning'
        });
      ) #
    else
      $rootScope.alerts.push({
        msg     : $filter("translate")("Register please recheck data ?"),
        type    : 'danger'
      });
);


###
 Account
###
app.controller('AccountCtrl',($scope,$rootScope,$state,$filter,$location,AccountProfileApi,$timeout)->

  $scope.isActive = (route)->
    page  = $location.$$path.split('/')[2] || 'home';
    route == page;

  AccountProfileApi.get({},(data)->
    $scope.data = data
  );


  if !$state.params.page
    $rootScope.pageTitle  = $filter('translate')('Home - Account');

    $scope.saveContact  = (valid) ->
      if (valid)
        AccountProfileApi.update({data : $scope.data}
        ,(data)-> # Update success
          $rootScope.alerts.push({
            msg   : $filter('translate')('Save profile success')
            type  : 'success'
          });
          $timeout(->
            $rootScope.emptyAlert();
          ,1000 * 2);
        ,(err)-> # Update error
          $rootScope.alerts.push({
            msg   : $filter('translate')('Profile please recheck data?')
            type  : 'danger'
          });
        );


  if $state.params.page == 'password'
    $rootScope.pageTitle      = $filter('translate')('Password - Account');
    $scope.savePassword  = (valid) ->
      if (valid)
        AccountProfileApi.password({ password : $scope.newPassword, oldPassword : $scope.oldPassword }
        ,(data)-> # success
          $rootScope.alerts.push({
            msg   : $filter('translate')('Save password success')
            type  : 'success'
          });
          $timeout(->
            $rootScope.emptyAlert();
          ,1000 * 2);
        , (err)-> # error
          $rootScope.alerts.push({
            msg   : $filter('translate')('Old password incorrect')
            type  : 'danger'
          })
        );
      else
        $rootScope.alerts.push({
          msg   : $filter('translate')('Password please recheck data')
          type  : 'danger'
        })


  if $state.params.page == 'favorites'
    $rootScope.pageTitle      = $filter('translate')('Favorites - Account')

  if $state.params.page == 'history'
    $rootScope.pageTitle      = $filter('translate')('History - Account')

)