"use strict"

# Directives
app   = angular.module("account.directives", []);

###
  Login form
###
app.directive('loginForm',->
  {
    scope       : {}
    templateUrl : 'modules/account/views/_login.html',
    controller  : "LoginFormCtrl"
  }
)

###
  Register form
###
app.directive('registerForm',->
  {
    scope       : {}
    templateUrl : 'modules/account/views/_register.html',
    controller  : "RegisterFormCtrl"
  } # Return
);

###
  Login out and redirect to home
###

app.directive('userLogout',['$window','$location','AuthenticationService','$rootScope',($window,$location,AuthenticationService,$scope)->
  {
    link : (scope,element,attr)->
      element.bind('click',->
        delete $window.sessionStorage.auth;
        delete $window.sessionStorage.level;
        AuthenticationService.isLogged  = false;
        AuthenticationService.level     = 'guest';
        $location.path("/home");
        $scope.$apply();
      );
  }
]);


# Check email unique email
app.directive('emailUnique', ['AccountUsersApi',(AccountUsersApi) ->
  return {
  require: 'ngModel',
  link: (scope, ele, attr,ctrl) ->
    ele.on('blur',()->
      AccountUsersApi.emailUnique({
          email : ele.val()
        },(data)->
          ctrl.$setValidity('unique', false)  if data.unique == false
          ctrl.$setValidity('unique', true)  if data.unique == true
      );
    )
  }
]);


# Check username unique
app.directive('usernameUnique', ['AccountUsersApi',(AccountUsersApi) ->
  return {
  require: 'ngModel',
  link: (scope, ele, attr,ctrl) ->
    ele.on('blur',()->
      AccountUsersApi.usernameUnique({
          username : ele.val()
        },(data)->
          ctrl.$setValidity('unique', false) if data.unique == false
          ctrl.$setValidity('unique', true) if data.unique == true
      );
    )
  }
]);

# Compare value
app.directive('equals',->
  {
    restrict: "A" # only activate on element attribute
    require: "?ngModel" # get a hold of NgModelController
    link: (scope, elem, attrs, ngModel) ->
      return  unless ngModel # do nothing if no ng-model

      # watch own value and re-validate on change
      scope.$watch attrs.ngModel, ->
        validate()
        return

      # observe the other value and re-validate on change
      attrs.$observe "equals", (val) ->
        validate()
        return

      validate = ->
        # values
        val1 = ngModel.$viewValue
        val2 = attrs.equals
        # set validity
        ngModel.$setValidity "equals", val1 is val2
        return

      return
  }
)