"use strict"

# Module
app = angular.module("myApp.translates", ['pascalprecht.translate'])

app.config(['$translateProvider',($translateProvider)->

  $translateProvider.useStaticFilesLoader({
    prefix: '/languages/',
    suffix: '.json'
  });

  $translateProvider.preferredLanguage('th');
]);