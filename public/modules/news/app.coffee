'use strict';

app = angular.module('myApp.news', [
  'news.controllers',
  'news.routes',
]);