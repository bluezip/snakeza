"use strict"

# Routes
app   = angular.module("news.routes", [])

app.config(['$stateProvider',($stateProvider)->

  $stateProvider
  .state('news',{
    url         : '/news',
    templateUrl : 'modules/news/views/index.html'
    controller  : 'NewsHomeCtrl'
  })
]);