"use strict"

# Controllers
app = angular.module("template.controllers", []);

app.controller('NavTopCtrl',($scope,CategoryApi)->
  CategoryApi.query({}
  ,(data)->
    data        =  _.where(data,{main : true})
    data          =  _.sortBy(data,(val)->
      return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig,'').charCodeAt(0));
    );
    $scope.list   = data;
  ,(err)->

  );
);
