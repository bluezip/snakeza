'use strict';

app = angular.module('myApp.template', [
  'template.controllers',
  'template.directives'
]);