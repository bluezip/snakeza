"use strict"

# Directives
app   = angular.module("template.directives", []);

# Top menu
app.directive('navTop',->
  {
  scope: {},
  templateUrl: 'modules/template/views/_top.html',
  controller: "NavTopCtrl"
  }
);


# Top menu
app.directive('footer',->
  {
    scope: {},
    templateUrl: 'modules/template/views/_footer.html'
  }
);