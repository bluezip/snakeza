'use strict';

app = angular.module('myApp', [
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'ngAnimate',
  'angular-redactor',

  'myApp.routes',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'myApp.translates',

  'myApp.admin',
  'myApp.account',
  'myApp.error',
  'myApp.news',
  'myApp.template',
  'myApp.home',
  'templates-main',
]);

# Check access
app.run ($rootScope, $location, AuthenticationService) ->

  $rootScope.$on("$stateChangeStart", (event, toState, toParams, fromState, fromParams) ->
    # Check level
    if toState.access?.level? && !_.isArray(toState.access.level)
      $location.path("/error/page-not-allow") if toState.access.level != AuthenticationService.level

    # Check level
    if toState.access?.level? && _.isArray(toState.access.level)
      $location.path("/error/page-not-allow") if _.indexOf(toState.access.level,AuthenticationService.level) == -1

    # Check login
    if toState.access?.requiredLogin == true
      $location.path "/login" if toState.access.requiredLogin  != AuthenticationService.isLogged

    $rootScope.alerts = []; # Remove notice
  );