"use strict"

# Service
app   = angular.module("myApp.services", ['ngResource']);


app.factory('apiRoot',->
  jQuery('config').attr('base-api');
);


###
  Auth check
###
app.factory "AuthenticationService", ->
  auth  = isLogged  : false;
  level = level     : 'guest';


# Create auth
app.factory('authInterceptor',($rootScope, $q, $window) ->
  return {
    request:(config) ->
      config.headers = config.headers || {};
      if ($window.sessionStorage.auth)
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.auth;
      return config;
    ,
    response: (response) ->
      return response || $q.when(response);
  }
);

# Create token
app.factory('applicationInterceptor',($q) ->
  return {
    request:(config) ->
      config.headers = config.headers || {};
      if angular.isFunction(jQuery)
        config.headers.app_token  = jQuery('config').attr('token');
      else
        throw "Not find jQuery...";
      return config;
    ,
    response: (response) ->
      return response || $q.when(response);
  }
);


###
  Set header before send header
###
app.config(($httpProvider)->
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('applicationInterceptor');
);

app.factory('CategoryApi',($resource,apiRoot)->
  $resource(apiRoot+'/category',{},{
    query : {
      isArray : true
    }
  });

);


###
  Socket
###

app.factory "socket", ($rootScope,apiRoot,$window) ->
  socket = io.connect(apiRoot.replace(/\/v\d+/i,''),{
    'query' : 'token='+ $window.sessionStorage.auth
  })

  on: (eventName, callback) ->
    socket.on eventName, ->
      args = arguments;
      $rootScope.$apply ->
        callback.apply socket, args

  emit: (eventName, data, callback) ->
    socket.emit eventName, data, ->
      args = arguments;
      $rootScope.$apply ->
        callback.apply socket, args  if callback