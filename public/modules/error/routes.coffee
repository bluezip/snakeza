"use strict"

# Routes
app   = angular.module("error.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider

  .state('/error',{
      url         : '/error',
      templateUrl : 'modules/error/views/index.html'
    })

  .state('/error.page-not-allow',{
      url : '/page-not-allow',
      views : {
        'content' : {
          templateUrl : 'modules/error/views/page-not-allow.html'
        }
      }
    })

]);