'use strict';

app = angular.module('myApp.error', [
  'error.routes',
  'error.controllers'
]);