angular.module('templates-main', ['modules/account/views/_login.html', 'modules/account/views/_register.html', 'modules/account/views/account.html', 'modules/account/views/index.html', 'modules/account/views/register.html', 'modules/admin/views/categories/_form.html', 'modules/admin/views/categories/index.html', 'modules/admin/views/home/index.html', 'modules/admin/views/html/_left-menu.html', 'modules/admin/views/user/create.html', 'modules/admin/views/user/index.html', 'modules/admin/views/user/update.html', 'modules/error/views/index.html', 'modules/error/views/page-not-allow.html', 'modules/home/views/index.html', 'modules/news/views/index.html', 'modules/template/views/_footer.html', 'modules/template/views/_top.html']);

angular.module("modules/account/views/_login.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/account/views/_login.html",
    "<div class=\"col-xs-8 col-xs-push-2\" ng-show=\"!$root.isLogin()\">\n" +
    "    <h3>{{'Login' | translate}}</h3>\n" +
    "    <form name=\"userLoginForm\" ng-submit=\"loginForm(userLoginForm.$valid)\" novalidate>\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Email' | translate}} :\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"email\" name=\"email\" ng-model=\"email\"  ng-required/>\n" +
    "\n" +
    "            <div class=\"error\" ng-show=\"userLoginForm.email.$dirty && userLoginForm.email.$invalid\">\n" +
    "                <small class=\"error\" ng-show=\"userLoginForm.email.$error.required\">\n" +
    "                    {{'Email not null' | translate }}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userLoginForm.email.$error.email\">\n" +
    "                    {{ 'Email is invalid' | translate }}\n" +
    "                </small>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Password' | translate }} :\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"password\" name=\"password\" ng-model=\"password\" ng-minlength=6 ng-maxlength=32  ng-required/>\n" +
    "            <div class=\"error\" ng-show=\"userLoginForm.password.$dirty && userLoginForm.password.$invalid\">\n" +
    "                <small class=\"error\" ng-show=\"userLoginForm.password.$error.required\">\n" +
    "                    {{'Password is blank' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userLoginForm.password.$error.minlength\">\n" +
    "                    {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userLoginForm.password.$error.maxlength\">\n" +
    "                    {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                </small>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-8 col-xs-push-4\">\n" +
    "            <button type=\"submit\" class=\"btn btn-default\">{{ 'Login' | translate }}</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);

angular.module("modules/account/views/_register.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/account/views/_register.html",
    "<div class=\"col-xs-8 col-xs-push-2\" ng-show=\"!$root.isLogin()\">\n" +
    "    <h3>{{'Register' | translate }}</h3>\n" +
    "    <form name=\"userRegisterForm\" ng-submit=\"registerForm(userRegisterForm.$valid)\"  novalidate>\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Email' | translate }} :\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"email\" name=\"email\" ng-model=\"email\"  ng-required email-unique />\n" +
    "            <div class=\"error\" ng-show=\"userRegisterForm.email.$dirty && userRegisterForm.email.$invalid\">\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.email.$error.required\">\n" +
    "                    {{'Email is blank' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.email.$error.email\">\n" +
    "                    {{'Invalid email format' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.email.$error.unique\">\n" +
    "                    {{'Has anyone used this email' | translate}}\n" +
    "                </small>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Username' | translate }} :\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"text\" name=\"username\" ng-model=\"username\" ng-minlength=6 ng-maxlength=32 ng-required=\"\" username-unique=\"\" ng-pattern=\"/^[A-Za-z0-9]+$/i\" />\n" +
    "\n" +
    "\n" +
    "            <div class=\"error\" ng-show=\"userRegisterForm.username.$dirty && userRegisterForm.username.$invalid\">\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.username.$error.pattern\">\n" +
    "                    {{'Username is not valid (Only A-Z, 0-9, -, _)' | translate}}\n" +
    "                </small>\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.username.$error.required\">\n" +
    "                    {{'Username is blank' | translate }}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.username.$error.minlength\">\n" +
    "                    {{'Username is greater than or equal to 6 characters' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.username.$error.maxlength\">\n" +
    "                    {{'Maximum username must not exceed 32 characters' | translate}}\n" +
    "                </small>\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.username.$error.unique\">\n" +
    "                    {{'Has anyone used this username' | translate}}\n" +
    "                </small>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Password' | translate }} :\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"password\" name=\"password\" ng-model=\"password\"  ng-minlength=\"6\" ng-maxlength=\"32\" ng-required=\"\" />\n" +
    "            <div class=\"error\" ng-show=\"userRegisterForm.password.$dirty && userRegisterForm.password.$invalid\">\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.password.$error.required\">\n" +
    "                    {{'Password is blank' | translate }}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.password.$error.minlength\">\n" +
    "                    {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                </small>\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.password.$error.maxlength\">\n" +
    "                    {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                </small>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "\n" +
    "        <div class=\"col-xs-4 text-right\">\n" +
    "            {{'Confirm' | translate }} :\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <input type=\"password\" name=\"rePassword\" ng-model=\"rePassword\" ng-required=\"\" ng-minlength=\"6\" ng-maxlength=\"32\"  equals=\"{{password}}\" />\n" +
    "\n" +
    "            <div class=\"error\" ng-show=\"userRegisterForm.rePassword.$dirty && userRegisterForm.rePassword.$invalid\">\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.rePassword.$error.required\">\n" +
    "                    {{'Password is blank' | translate }}\n" +
    "                </small>\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.rePassword.$error.minlength\">\n" +
    "                    {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                </small>\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.rePassword.$error.maxlength\">\n" +
    "                    {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                </small>\n" +
    "\n" +
    "                <small class=\"error\" ng-show=\"userRegisterForm.rePassword.$error.equals\">\n" +
    "                    {{'Password not match' | translate}}\n" +
    "                </small>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-8 col-xs-push-4\">\n" +
    "            <button type=\"submit\" class=\"btn btn-default\">{{ 'Register' | translate}}</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);

angular.module("modules/account/views/account.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/account/views/account.html",
    "<div ng-show=\"$root.isLogin()\" class=\"account\">\n" +
    "    <div class=\"col-xs-3\">\n" +
    "        <ul class=\"nav nav-pills nav-stacked menu\">\n" +
    "            <li ng-class=\"{'active' : isActive('home') }\"><a href=\"/account\">{{'Account - Page' | translate }}</a></li>\n" +
    "            <li ng-class=\"{'active' : isActive('favorites') }\"><a href=\"/account/favorites\">{{'Favorites - Page' | translate}}</a></li>\n" +
    "            <li ng-class=\"{'active' : isActive('history') }\"><a href=\"/account/history\">{{'History - Page' | translate}}</a></li>\n" +
    "            <li ng-class=\"{'active' : isActive('message') }\"><a href=\"/account/message\">{{'Message - Page' | translate}}</a></li>\n" +
    "            <li ng-class=\"{'active' : isActive('password') }\"><a href=\"/account/password\">{{'Password - Page' | translate}}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-9\">\n" +
    "        <div ng-show=\"isActive('home')\">\n" +
    "            <div class=\"head-title\">\n" +
    "                <h3>{{'Account information' | translate}}</h3>\n" +
    "            </div>\n" +
    "\n" +
    "            <form name=\"contact\" ng-submit=\"saveContact(contact.$valid)\"  novalidate>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Email:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\"><strong>{{data.email}}</strong></div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Username:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\"><strong>{{data.username}}</strong></div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Name:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"text\" ng-model=\"data.name.before\">\n" +
    "                    <input type=\"text\" ng-model=\"data.name.name\">\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Last name:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"text\" ng-model=\"data.name.last_name\">\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Address:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <textarea ng-model=\"data.contact.address\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Province:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"text\" ng-model=\"data.contact.province\" />\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Postcode:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"text\" name=\"postcode\" ng-model=\"data.contact.postcode\" ng-pattern=\"/^\\d{5}$/i\" />\n" +
    "                    <div class=\"error\" ng-show=\"contact.postcode.$dirty && contact.postcode.$invalid\">\n" +
    "                        <small class=\"error\" ng-show=\"contact.postcode.$error.pattern\">\n" +
    "                            {{'Postcode is invalid' | translate}}\n" +
    "                        </small>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Telephone:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"text\" ng-model=\"data.contact.telephone\" />\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-9 col-xs-push-3\"><button type=\"submit\" class=\"btn btn-default\">{{'Submit' | translate }}</button></div>\n" +
    "            </form>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-show=\"isActive('favorites')\">\n" +
    "            <div class=\"head-title\">\n" +
    "                <h3>{{'Favorites' | translate}}</h3>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-show=\"isActive('history')\">\n" +
    "            <div class=\"head-title\">\n" +
    "                <h3>{{'History' | translate}}</h3>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-show=\"isActive('message')\">\n" +
    "            <div class=\"head-title\">\n" +
    "                <h3>{{'Message' | translate}}</h3>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-show=\"isActive('password')\">\n" +
    "            <div class=\"head-title\">\n" +
    "                <h3>{{'Password' | translate}}</h3>\n" +
    "            </div>\n" +
    "\n" +
    "            <form name=\"password\" ng-submit=\"savePassword(password.$valid)\"  novalidate>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Old password:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"password\" name=\"oldPassword\" ng-model=\"oldPassword\" ng-minlength=6 ng-maxlength=32  required/>\n" +
    "                    <div class=\"error\" ng-show=\"password.oldPassword.$dirty && password.oldPassword.$invalid\">\n" +
    "                        <small class=\"error\" ng-show=\"password.oldPassword.$error.required\">\n" +
    "                            {{'Password is blank' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.oldPassword.$error.minlength\">\n" +
    "                            {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.oldPassword.$error.maxlength\">\n" +
    "                            {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                        </small>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'New password:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"password\" name=\"newPassword\" ng-model=\"newPassword\" ng-minlength=6 ng-maxlength=32  required/>\n" +
    "                    <div class=\"error\" ng-show=\"password.newPassword.$dirty && password.newPassword.$invalid\">\n" +
    "                        <small class=\"error\" ng-show=\"password.newPassword.$error.required\">\n" +
    "                            {{'Password is blank' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.newPassword.$error.minlength\">\n" +
    "                            {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.newPassword.$error.maxlength\">\n" +
    "                            {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                        </small>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-3 text-right\">{{'Confirm password:' | translate }}</div>\n" +
    "                <div class=\"col-xs-9\">\n" +
    "                    <input type=\"password\" name=\"confirmPassword\" ng-model=\"confirmPassword\" ng-minlength=6 ng-maxlength=32  required equals=\"{{newPassword}}\"/>\n" +
    "                    <div class=\"error\" ng-show=\"password.confirmPassword.$dirty && password.confirmPassword.$invalid\">\n" +
    "                        <small class=\"error\" ng-show=\"password.confirmPassword.$error.required\">\n" +
    "                            {{'Password is blank' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.confirmPassword.$error.minlength\">\n" +
    "                            {{'Password is greater than or equal to 6 characters' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.confirmPassword.$error.maxlength\">\n" +
    "                            {{'Maximum password must not exceed 32 characters' | translate}}\n" +
    "                        </small>\n" +
    "                        <small class=\"error\" ng-show=\"password.confirmPassword.$error.equals\">\n" +
    "                            {{'Password not match' | translate}}\n" +
    "                        </small>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-9 col-xs-push-3\"><button type=\"submit\" class=\"btn btn-default\">{{'Submit' | translate }}</button></div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("modules/account/views/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/account/views/index.html",
    "<div nav-top></div>\n" +
    "<div class=\"container\">\n" +
    "    <alert ng-repeat=\"alert in alerts\" type=\"{{alert.type}}\" close=\"$parent.closeAlert($index)\">{{alert.msg}}</alert>\n" +
    "\n" +
    "    <div ui-view=\"edit\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>\n" +
    "<div footer></div>\n" +
    "");
}]);

angular.module("modules/account/views/register.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/account/views/register.html",
    "<!--\n" +
    "    module  : account\n" +
    "    page    : register\n" +
    "-->\n" +
    "\n" +
    "<div nav-top></div>\n" +
    "\n" +
    "<div class=\"container account-register\">\n" +
    "    <alert ng-repeat=\"alert in alerts\" type=\"{{alert.type}}\" close=\"$root.closeAlert($index)\">{{alert.msg}}</alert>\n" +
    "\n" +
    "    <div>\n" +
    "        <div class=\"col-xs-12 col-lg-6 col-lg-push-6\">\n" +
    "            <div login-form></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-12 col-lg-6 col-lg-pull-6\">\n" +
    "            <div register-form></div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>\n" +
    "<div footer></div>");
}]);

angular.module("modules/admin/views/categories/_form.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/categories/_form.html",
    "<div class=\"admin-categories-module\">\n" +
    "    <div class=\"col-xs-10\">\n" +
    "        <div class=\"form-header\">\n" +
    "            <h3 class=\"modal-title\" ng-show=\"!update\">{{'Create category' | translate}}</h3>\n" +
    "            <h3 class=\"modal-title\" ng-show=\"update\">{{'Update category' | translate}}</h3>\n" +
    "        </div>\n" +
    "        <div class=\"form-body\">\n" +
    "            <alert ng-repeat=\"alert in errors\" type=\"{{alert.type}}\" close=\"$root.closeErrors($index)\">{{alert.msg}}</alert>\n" +
    "\n" +
    "            <form name=\"categories\" ng-submit=\"save(categories.$valid)\">\n" +
    "                <div class=\"col-xs-4\">\n" +
    "                    {{'Name' | translate}}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input type=\"text\" ng-model=\"data.name\" class=\"form-control\" />\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-4\">\n" +
    "                    {{'Url' | translate }}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input type=\"text\" ng-model=\"data.url\" class=\"form-control\" />\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-4\" ng-hide=\"data.main == true\">\n" +
    "                    {{'Sub category' | translate}}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\" ng-hide=\"data.main == true\">\n" +
    "                    <select ng-model=\"parent\" ng-change=\"data.url = parent\" class=\"form-control\">\n" +
    "                        <option value=\"\">{{'Select' | translate}}</option>\n" +
    "                        <option value=\"{{item.url}}\" ng-repeat=\"item in list\" ng-selected=\"selectItem(item.url) == true\">{{item.name}}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\" ng-hide=\"data.main == true\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-4\">\n" +
    "                    {{'Main category' | translate }}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input type=\"checkbox\" ng-checked=\"data.main\" ng-model=\"data.main\" ng-click=\"data.url =''\">\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "                <div class=\"col-xs-4\">\n" +
    "                    {{'Keyword' | translate }}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <input ng-model=\"data.keyword\" type=\"text\"  class=\"form-control\" />\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-4\">\n" +
    "                    {{'Description' | translate }}\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <textarea ng-model=\"data.description\"  class=\"form-control\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-4\"></div>\n" +
    "                <div class=\"col-xs-6\">\n" +
    "                    <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i> {{'Save' | translate }}</button>\n" +
    "                </div>\n" +
    "                <div class=\"clearfix\"></div>\n" +
    "\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div><!-- .col-xs-10 -->\n" +
    "</div> <!-- .admin-categoriew-module\n" +
    "\n" +
    "");
}]);

angular.module("modules/admin/views/categories/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/categories/index.html",
    "<div class=\"admin-categories-module\">\n" +
    "\n" +
    "    <div class=\"col-xs-12 create-link\">\n" +
    "        <div class=\"pull-right\">\n" +
    "            <a href=\"/admin/categories/create\"><i class=\"fa fa-pencil\"></i> {{'Create categories' | translate}}</a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "    <div ng-repeat=\"item in list\">\n" +
    "        <div class=\"list col-xs-12\">\n" +
    "            <div class=\"col-xs-9\">\n" +
    "                <div class=\"col-xs-7\">\n" +
    "                    <sapn>{{ $index + 1}}</sapn>\n" +
    "                    <span class=\"title\">{{ item.name }}</span>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-5\">\n" +
    "                    <span class=\"url\">{{ item.url }}</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-3  link text-right\">\n" +
    "                <a href=\"/admin/categories/update/{{item._id}}\"><i class=\"fa fa-pencil\"></i> {{'Update' | translate}}</a>\n" +
    "                <a href=\"/admin/categories/sub{{item.url}}\"  ng-show=\"page == null\"><i class=\"fa fa-folder-open\"></i> {{'Sub category' | translate}}</a>\n" +
    "                <a ng-click=\"delete(item)\"><i class=\"fa fa-trash-o\"></i> {{'Delete' | translate}}</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"clearfix\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "</div> <!-- admin-category -->\n" +
    "\n" +
    "<div class=\"clearfix\"></div>");
}]);

angular.module("modules/admin/views/home/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/home/index.html",
    "<div nav-top></div>\n" +
    "\n" +
    "<div class=\"admin\">\n" +
    "\n" +
    "    <div class=\"left-menu\" admin-menu></div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"content container\">\n" +
    "        <alert ng-repeat=\"alert in $root.alerts\" type=\"{{alert.type}}\" close=\"$root.closeAlert($index)\">{{alert.msg}}</alert>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"content container\" ui-view=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("modules/admin/views/html/_left-menu.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/html/_left-menu.html",
    "<ul class=\"nav nav-pills nav-stacked\">\n" +
    "    <li ng-class=\"{'active' : isActive('/admin')}\"><a href=\"/admin\">{{'Home - Admin' | translate}}</a></li>\n" +
    "    <li ng-class=\"{'active' : isActive('/admin/user')}\"><a href=\"/admin/user\">{{'User - Admin' | translate}}</a></li>\n" +
    "    <li ng-class=\"{'active' : isActive('/admin/categories')}\"><a href=\"/admin/categories\">{{'Categories - Admin' | translate}}</a></li>\n" +
    "</ul>");
}]);

angular.module("modules/admin/views/user/create.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/user/create.html",
    "Create");
}]);

angular.module("modules/admin/views/user/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/user/index.html",
    "<div class=\"admin-user\">\n" +
    "    <div class=\"col-xs-12 create-link\">\n" +
    "        <div class=\"pull-right\">\n" +
    "            <a href=\"/admin/user/create\"><i class=\"fa fa-pencil\"></i> {{'Create categories' | translate}}</a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"clearfix\"></div>\n" +
    "\n" +
    "\n" +
    "    <div ng-repeat=\"item in list\">\n" +
    "        <div class=\"list col-xs-12\">\n" +
    "            <div class=\"col-xs-9\">\n" +
    "                <div class=\"col-xs-7\">\n" +
    "                    <sapn>{{ $index + 1}}</sapn>\n" +
    "                    <span class=\"title\">{{ item.email }}</span>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-5\">\n" +
    "                    <span class=\"url\">{{ item.username }}</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-3  link text-right\">\n" +
    "                <a href=\"/admin/user/update/{{item._id}}\"><i class=\"fa fa-pencil\"></i> {{'Update' | translate}}</a>\n" +
    "                <a ng-click=\"delete(item)\"><i class=\"fa fa-trash-o\"></i> {{'Delete' | translate}}</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"clearfix\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "</div> <!-- admin-category -->\n" +
    "\n" +
    "<div class=\"clearfix\"></div>");
}]);

angular.module("modules/admin/views/user/update.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/admin/views/user/update.html",
    "Update");
}]);

angular.module("modules/error/views/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/error/views/index.html",
    "<div nav-top></div>\n" +
    "\n" +
    "<div class=\"container\" ui-view=\"content\"></div>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>\n" +
    "<div footer></div>\n" +
    "\n" +
    "");
}]);

angular.module("modules/error/views/page-not-allow.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/error/views/page-not-allow.html",
    "<h1>Method not allow</h1>");
}]);

angular.module("modules/home/views/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/home/views/index.html",
    "<div nav-top></div>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>\n" +
    "<div footer></div>");
}]);

angular.module("modules/news/views/index.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/news/views/index.html",
    "<div nav-top></div>\n" +
    "\n" +
    "<div class=\"container\">\n" +
    "    <h1>News</h1>\n" +
    "    <textarea ng-model=\"content\" redactor></textarea>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>\n" +
    "<div footer></div>");
}]);

angular.module("modules/template/views/_footer.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/template/views/_footer.html",
    "<div class=\"container\">\n" +
    "    <span app-version></span>\n" +
    "</div>");
}]);

angular.module("modules/template/views/_top.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/template/views/_top.html",
    "<nav class=\"navbar navbar-default\" role=\"navigation\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "        <!-- Brand and toggle get grouped for better mobile display -->\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n" +
    "                <span class=\"sr-only\">Toggle navigation</span>\n" +
    "                <span class=\"icon-bar\"></span>\n" +
    "                <span class=\"icon-bar\"></span>\n" +
    "                <span class=\"icon-bar\"></span>\n" +
    "            </button>\n" +
    "            <a class=\"navbar-brand\" href=\"/home\">Snakeza</a>\n" +
    "        </div>\n" +
    "\n" +
    "        <!-- Collect the nav links, forms, and other content for toggling -->\n" +
    "        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li><a href=\"/board\">{{'Web board' | translate}}</a></li>\n" +
    "                <li class=\"dropdown\">\n" +
    "                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{'Categories' | translate}}<b class=\"caret\"></b></a>\n" +
    "                    <ul class=\"dropdown-menu\">\n" +
    "                        <li ng-repeat=\"item in list\">\n" +
    "                            <a href=\"{{item.url}}\">{{item.name}}</a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <form class=\"navbar-form navbar-left\" role=\"search\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"text\" class=\"form-control\" placeholder=\"{{'Search' | translate }}\">\n" +
    "                </div>\n" +
    "                <button type=\"submit\" class=\"btn btn-default\">{{'Search submit' | translate}}</button>\n" +
    "            </form>\n" +
    "            <ul class=\"nav navbar-nav navbar-right\">\n" +
    "                <li class=\"dropdown\">\n" +
    "                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{'Account' | translate}} <b class=\"caret\"></b></a>\n" +
    "                    <ul class=\"dropdown-menu\">\n" +
    "                        <li><a href=\"/account\" ng-show=\"$root.isLogin()\">{{'Account - Page' | translate}}</a></li>\n" +
    "                        <li><a href=\"/account/favorites\" ng-show=\"$root.isLogin()\">{{'Favorites - Page'| translate}}</a></li>\n" +
    "                        <li><a href=\"/account/history\" ng-show=\"$root.isLogin()\">{{'History - Page'| translate}}</a></li>\n" +
    "                        <li><a href=\"/account/message\" ng-show=\"$root.isLogin()\">{{'Message - Page'| translate}}</a></li>\n" +
    "\n" +
    "                        <li class=\"divider\" ng-show=\"$root.isLogin() && $root.isAdmin()\"></li>\n" +
    "                        <li><a href=\"/admin\" ng-show=\"$root.isLogin() && $root.isAdmin()\">{{'Admin - Page'| translate}}</a></li>\n" +
    "\n" +
    "                        <li class=\"divider\" ng-show=\"$root.isLogin()\"></li>\n" +
    "                        <li><a href=\"/account/password\" ng-show=\"$root.isLogin()\">{{'Password - Page' | translate}}</a></li>\n" +
    "                        <li ng-show=\"$root.isLogin()\"><a user-logout href=\"/home\">{{'Logout' | translate}}</a></li>\n" +
    "                        <li ng-show=\"!$root.isLogin()\"><a href=\"/login\">{{'Login' | translate}}</a></li>\n" +
    "                        <li ng-show=\"!$root.isLogin()\"><a href=\"/register\">{{'Register' | translate}}</a></li>\n" +
    "                    </ul>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div><!-- /.navbar-collapse -->\n" +
    "    </div><!-- /.container-fluid -->\n" +
    "</nav>\n" +
    "\n" +
    "<div class=\"clearfix\"></div>");
}]);
