'use strict';

app = angular.module('myApp.account', [
  'account.controllers',
  'account.routes',
  'account.directives',
  'account.services',
]);
"use strict"

# Controllers
app = angular.module("account.controllers", []);


###
  For directives (loginForm)
###
app.controller('LoginFormCtrl',($scope, AccountAuthApi, $window, $rootScope,$filter,AuthenticationService,$location)->
  $scope.loginForm = (valid)->
    if(valid)
      AccountAuthApi.login({
          email     : $scope.email
          password  : $scope.password
        }, (data)-> # success
          AuthenticationService.isLogged  = true;
          $window.sessionStorage.level    = AuthenticationService.level     = data.level;
          $window.sessionStorage.auth     = data.auth;
          $scope.email  = $scope.password   = '' # set null data
          $rootScope.alerts.push({
            msg     : $filter('translate')('Login already'),
            type    : 'success'
          });
          $location.path("/account");
      ,(err) -> # error
        AuthenticationService.isLogged  = false;
        delete $window.sessionStorage.level;
        delete $window.sessionStorage.auth;
        $rootScope.alerts.push({
          msg     : $filter('translate')('Email or password is incorrect.'),
          type    : 'danger'
        });
      )# end Auth.login()
    else
      $rootScope.alerts.push({
        msg     : $filter("translate")("Login please recheck data ?"),
        type    : 'danger'
      });
)


###
 Register form
###

app.controller('RegisterFormCtrl',($scope, AccountAuthApi, AccountUsersApi, $window, $rootScope,$filter,AuthenticationService,$location)->

  # Auto login
  autoLogin   = ->
    AccountAuthApi.login({
        email     : $scope.email
        password  : $scope.password
      }
    ,(data)-> # success
      AuthenticationService.isLogged  = true;
      $window.sessionStorage.auth     = data.auth;
      $location.path("/account");
    ,(err)-> # error
      AuthenticationService.isLogged  = false;
      delete $window.sessionStorage.level;
      delete $window.sessionStorage.auth;
    )# end Auth.login()


  $scope.registerForm = (valid) ->
    if(valid)
      AccountUsersApi.register({
          email     : $scope.email,
          password  : $scope.password,
          username  : $scope.username
        }
      ,(data)-> # success
        autoLogin();
        $scope.email  = $scope.rePassword  = $scope.password   = '' # set null data
        $rootScope.alerts.push({
          msg     : $filter('translate')('Already Registered'),
          type    : 'success'
        });

      , (err)-> # error
        $rootScope.alerts.push({
          msg     : err.message,
          type    : 'warning'
        });
      ) #
    else
      $rootScope.alerts.push({
        msg     : $filter("translate")("Register please recheck data ?"),
        type    : 'danger'
      });
);


###
 Account
###
app.controller('AccountCtrl',($scope,$rootScope,$state,$filter,$location,AccountProfileApi,$timeout)->

  $scope.isActive = (route)->
    page  = $location.$$path.split('/')[2] || 'home';
    route == page;

  AccountProfileApi.get({},(data)->
    $scope.data = data
  );


  if !$state.params.page
    $rootScope.pageTitle  = $filter('translate')('Home - Account');

    $scope.saveContact  = (valid) ->
      if (valid)
        AccountProfileApi.update({data : $scope.data}
        ,(data)-> # Update success
          $rootScope.alerts.push({
            msg   : $filter('translate')('Save profile success')
            type  : 'success'
          });
          $timeout(->
            $rootScope.emptyAlert();
          ,1000 * 2);
        ,(err)-> # Update error
          $rootScope.alerts.push({
            msg   : $filter('translate')('Profile please recheck data?')
            type  : 'danger'
          });
        );


  if $state.params.page == 'password'
    $rootScope.pageTitle      = $filter('translate')('Password - Account');
    $scope.savePassword  = (valid) ->
      if (valid)
        AccountProfileApi.password({ password : $scope.newPassword, oldPassword : $scope.oldPassword }
        ,(data)-> # success
          $rootScope.alerts.push({
            msg   : $filter('translate')('Save password success')
            type  : 'success'
          });
          $timeout(->
            $rootScope.emptyAlert();
          ,1000 * 2);
        , (err)-> # error
          $rootScope.alerts.push({
            msg   : $filter('translate')('Old password incorrect')
            type  : 'danger'
          })
        );
      else
        $rootScope.alerts.push({
          msg   : $filter('translate')('Password please recheck data')
          type  : 'danger'
        })


  if $state.params.page == 'favorites'
    $rootScope.pageTitle      = $filter('translate')('Favorites - Account')

  if $state.params.page == 'history'
    $rootScope.pageTitle      = $filter('translate')('History - Account')

)
"use strict"

# Directives
app   = angular.module("account.directives", []);

###
  Login form
###
app.directive('loginForm',->
  {
    scope       : {}
    templateUrl : 'modules/account/views/_login.html',
    controller  : "LoginFormCtrl"
  }
)

###
  Register form
###
app.directive('registerForm',->
  {
    scope       : {}
    templateUrl : 'modules/account/views/_register.html',
    controller  : "RegisterFormCtrl"
  } # Return
);

###
  Login out and redirect to home
###

app.directive('userLogout',['$window','$location','AuthenticationService','$rootScope',($window,$location,AuthenticationService,$scope)->
  {
    link : (scope,element,attr)->
      element.bind('click',->
        delete $window.sessionStorage.auth;
        delete $window.sessionStorage.level;
        AuthenticationService.isLogged  = false;
        AuthenticationService.level     = 'guest';
        $location.path("/home");
        $scope.$apply();
      );
  }
]);


# Check email unique email
app.directive('emailUnique', ['AccountUsersApi',(AccountUsersApi) ->
  return {
  require: 'ngModel',
  link: (scope, ele, attr,ctrl) ->
    ele.on('blur',()->
      AccountUsersApi.emailUnique({
          email : ele.val()
        },(data)->
          ctrl.$setValidity('unique', false)  if data.unique == false
          ctrl.$setValidity('unique', true)  if data.unique == true
      );
    )
  }
]);


# Check username unique
app.directive('usernameUnique', ['AccountUsersApi',(AccountUsersApi) ->
  return {
  require: 'ngModel',
  link: (scope, ele, attr,ctrl) ->
    ele.on('blur',()->
      AccountUsersApi.usernameUnique({
          username : ele.val()
        },(data)->
          ctrl.$setValidity('unique', false) if data.unique == false
          ctrl.$setValidity('unique', true) if data.unique == true
      );
    )
  }
]);

# Compare value
app.directive('equals',->
  {
    restrict: "A" # only activate on element attribute
    require: "?ngModel" # get a hold of NgModelController
    link: (scope, elem, attrs, ngModel) ->
      return  unless ngModel # do nothing if no ng-model

      # watch own value and re-validate on change
      scope.$watch attrs.ngModel, ->
        validate()
        return

      # observe the other value and re-validate on change
      attrs.$observe "equals", (val) ->
        validate()
        return

      validate = ->
        # values
        val1 = ngModel.$viewValue
        val2 = attrs.equals
        # set validity
        ngModel.$setValidity "equals", val1 is val2
        return

      return
  }
)
"use strict"

# Routes
app   = angular.module("account.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider
  .state('register',{
    url         : '/register',
    templateUrl : 'modules/account/views/register.html',
    controller  : ($rootScope,$filter)->
      $rootScope.pageTitle       = $filter('translate')('Register');

  })

  .state('login',{
    url         : '/login',
    templateUrl : 'modules/account/views/register.html',
    controller  : ($scope,$rootScope,$filter)->
      $rootScope.pageTitle      = $filter('translate')('Login');
    })

  .state('account',{
    url : '/account',
    views : {
      ''  : {
        templateUrl   : 'modules/account/views/index.html',
      },
      'edit@account'  : {
        templateUrl   : 'modules/account/views/account.html',
        controller    : 'AccountCtrl'
      }
    },
    access: { requiredLogin: true }
  })

  .state('account.edit',{
    url : '/:page',
    views : {
      'edit@account'  : {
        templateUrl   : 'modules/account/views/account.html',
        controller    : 'AccountCtrl'
      }
    },
    access: { requiredLogin: true }
  })
]);
"use strict"

# Service
app   = angular.module("account.services", ['ngResource']);

# Auth url
app.factory("AccountAuthApi",($resource,apiRoot)->
  $resource(apiRoot+'/auth',{},{
    login : {
      url : apiRoot+'/auth'
      method : 'POST',
      params : {}
    }
  });
);


# User api url
app.factory("AccountUsersApi",($resource,apiRoot)->

  $resource(apiRoot+'/user',{}, {

  # Create account
    register  :{
      method  : 'POST'
    },

  # Check unique email
    emailUnique : {
      url     : apiRoot+'/user/email',
      method  : 'GET',
      params  : {
        email : '@email',
      }
    },

  # Check unique username
    usernameUnique : {
      url     : apiRoot+'/user/username',
      method  : 'GET',
      params  : {
        username  : '@username',
      }
    },
  });
);

###
  Profile api url
###
app.factory('AccountProfileApi',($resource,apiRoot)->
  $resource(apiRoot+'/profile',{}, {
    get : {
      method  : "GET",
      url     : apiRoot+'/profile/info',
      cache   : false,
    },
    update : {
      method  : "PUT",
      url     : apiRoot+'/profile/info',
    },
    password : {
      method  : "PUT",
      url     : apiRoot+'/profile/password',
    }
  });
)

'use strict';

app = angular.module('myApp.admin', [
  'admin.routes',
  'admin.controllers',
  'admin.directives',
  'admin.services',

  'admin.categoriesControllers',
  'admin.userControllers',
]);
"use strict"

# Controllers
app = angular.module("admin.categoriesControllers", []);

app.controller('AdminCategoriesCtrl',($scope,$filter,$rootScope,$state,AdminCategoriesApi,$modal)->
  $rootScope.pageTitle  = $filter('translate')('Categories - Admin');

  $scope.page = $state.params.page;

  AdminCategoriesApi.query({},(data)->
    if $state.params.page?
      data  =  _.where(data,{main : false });
      data  =  _.filter(data,(val)->
        url = $state.params.page
        rex = new RegExp('^\/'+url,'i');
        val.url.match(rex);
      );
    else
      data        =  _.where(data,{main : true})

#    data          =  _.sortBy(data,(val)-> return new Date(val.update_at).getTime() );
#    $scope.list   = data.reverse();

    data          =  _.sortBy(data,(val)->
      return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig,'').charCodeAt(0));
    );
    $scope.list   = data;
  );

  $scope.delete = (data) ->
    if(confirm($filter('translate')('Confirm delete?')))
      AdminCategoriesApi.delete({id : data._id}
      ,(data)->
        $state.go('admin.categories',{}, {reload: true});
      ,(err)->

      );
);



app.controller('AdminCategoriesSaveCtrl',($scope,$filter,AdminCategoriesApi,$state,$rootScope)->

  if $state.params.id?
    AdminCategoriesApi.get({id : $state.params.id}
    ,(data)->
      $scope.data = data;
    ,(err)->

    );
    $scope.update   = true;

  AdminCategoriesApi.query({},(data)->
    data          =  _.where(data,{main : true});
    data          = _.sortBy(data,(val)-> return new Date(val.update_at).getTime() );
    $scope.list   = data.reverse();
  );

  $scope.selectItem = (item)->
    url = $scope.data?.url?.split('/')[1];
    rex = new RegExp('^\/'+url,'i');
    if item.match(rex)
      return true;
    else
      return false;


  goto    = (data)->
    if data.main == true
      $state.go('admin.categories');
    else
      url = $scope.data.url?.split('/')[1];
      $state.go('admin.categories.sub',{page : url});

  error   = ->
    $rootScope.alerts.push({
      msg   : $filter('translate')('Please recheck data?'),
      type  : 'danger'
    });

  # save
  $scope.save = (vaild) ->
    if vaild
      if $state.params.id?
        AdminCategoriesApi.update({
            data  : $scope.data
            id    : $state.params.id
          }
        ,(data)-> # success
          goto($scope.data);
        ,(err)-> # error
          error();
        );
      else
        AdminCategoriesApi.save({data : $scope.data }
        ,(data)->
          goto($scope.data);
        ,(err)->
          error();
        );
    # end if
);
"use strict"

# Controllers
app = angular.module("admin.controllers", []);

# For directives admin-menu
app.controller('AdminMenuCtrl',($scope,$location)->

  $scope.isActive = (route) ->
    route.split('/')[2] == $location.$$path.split('/')[2]


);

app.controller('AdminHomeCtrl',($scope)->

);
"use strict"

# Directives
app   = angular.module("admin.directives", [])

app.directive('adminMenu',->
  {
    templateUrl : 'modules/admin/views/html/_left-menu.html',
    controller : 'AdminMenuCtrl'
  }
);
"use strict"

# Routes
app   = angular.module("admin.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider

  .state('admin', {
      url         : "/admin",
      templateUrl : "modules/admin/views/home/index.html",
      controller  : 'AdminHomeCtrl',
      access      : { requiredLogin: true , level : 'admin' }
    })

  .state('admin.categories', {
      url         : "/categories",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/index.html",
          controller  : 'AdminCategoriesCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin' }
    })

  .state('admin.categories.create', {
      url         : "/create",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/_form.html",
          controller  : 'AdminCategoriesSaveCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin'}
    })

  .state('admin.categories.update', {
      url         : "/update/:id",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/_form.html",
          controller  : 'AdminCategoriesSaveCtrl'
        }
      },
      access      : { requiredLogin: true , level : 'admin'}
    })

  .state('admin.categories.sub', {
      url         : "/sub/:page",
      views       : {
        'content@admin' : {
          templateUrl : "modules/admin/views/categories/index.html",
          controller  : 'AdminCategoriesCtrl',
          access      : { requiredLogin: true , level : 'admin'}
        }
      }
    })

  .state('admin.user',{
      url     : "/user",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/index.html",
          controller  : "AdminUserCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })

  .state('admin.user.create',{
      url     : "/create",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/create.html",
          controller  : "AdminUserCreateCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })

  .state('admin.user.update',{
      url     : "/update/:id",
      views   : {
        'content@admin' : {
          templateUrl : "modules/admin/views/user/update.html",
          controller  : "AdminUserUpdateCtrl"
        }
      },
      access      : { requiredLogin: true , level : ['admin','mod']}
    })
]);
"use strict"

# Service
app   = angular.module("admin.services", ['ngResource']);

app.factory('AdminCategoriesApi',($resource,apiRoot)->
  $resource(apiRoot+'/admin/category/:id',{
    id : '@id'
  },{
    query : {
      url     : apiRoot+'/category',
      method  : "GET",
      isArray : true,
      cache   : false,
    },
    save : {
      method : "POST",
    },
    update : {
      method : "PUT"
    },
    delete : {
      method : "DELETE"
    }
  });
);


app.factory('AdminUserApi',($resource,apiRoot)->
  $resource(apiRoot+'/admin/user/:id',{
    id : '@id'
  },{
    query : {
      url     : apiRoot+'/admin/user',
      method  : "GET",
      isArray : true
    },
    save : {
      method : "POST",
    },
    update : {
      method : "PUT"
    },
    delete : {
      method : "DELETE"
    }
  });
);
"use strict"

# Controllers
app = angular.module("admin.userControllers", []);

app.controller('AdminUserCtrl',(AdminUserApi,$scope,$window,$filter)->
  AdminUserApi.query({},(data)->
    $scope.list = data;
  ,(err)->

  );

  $scope.delete = (data) ->
    if(confirm($filter('translate')('Confirm delete?')))
      AdminUserApi.save({id : data._id}
      ,(data)->
        console.log('yyyy');
#        $window.location.reload();
      ,(err)->
        console.log('nnnn');
#        $window.location.reload();
      );
);

app.controller('AdminUserCreateCtrl',(AdminUserApi,$scope,$filter)->

);

app.controller('AdminUserUpdateCtrl',(AdminUserApi,$scope,$filter)->
  AdminUserApi.update({id : data._id}
  ,(data)->
    $scope.data = data;
  ,(err)->

  );
);
'use strict';

app = angular.module('myApp', [
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'ngAnimate',
  'angular-redactor',

  'myApp.routes',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'myApp.translates',

  'myApp.admin',
  'myApp.account',
  'myApp.error',
  'myApp.news',
  'myApp.template',
  'myApp.home',
  'templates-main',
]);

# Check access
app.run ($rootScope, $location, AuthenticationService) ->

  $rootScope.$on("$stateChangeStart", (event, toState, toParams, fromState, fromParams) ->
    # Check level
    if toState.access?.level? && !_.isArray(toState.access.level)
      $location.path("/error/page-not-allow") if toState.access.level != AuthenticationService.level

    # Check level
    if toState.access?.level? && _.isArray(toState.access.level)
      $location.path("/error/page-not-allow") if _.indexOf(toState.access.level,AuthenticationService.level) == -1

    # Check login
    if toState.access?.requiredLogin == true
      $location.path "/login" if toState.access.requiredLogin  != AuthenticationService.isLogged

    $rootScope.alerts = []; # Remove notice
  );
"use strict"

# Controllers
app = angular.module("myApp.controllers", [])


app.controller('MainCtrl',($rootScope,AuthenticationService)->
  $rootScope.pageTitle        = '';
  $rootScope.pageDescription  = '';
  $rootScope.pageKeyword      = '';

  $rootScope.alerts           = [];

  $rootScope.closeAlert       = (index) ->
    $rootScope.alerts.splice(index, 1);

  $rootScope.emptyAlert       = ->
    $rootScope.alerts         = [];


  # Check is admin
  $rootScope.isAdmin    = ->
    AuthenticationService.level    == 'admin';

  # Check is admin
  $rootScope.isMember    = ->
    AuthenticationService.level    == 'member';

  # Check is guest
  $rootScope.isGuest    = ->
    AuthenticationService.level    == 'guest';

  # Check login
  $rootScope.isLogin    = ->
    AuthenticationService.isLogged;

)
"use strict"

# Directives
app   = angular.module("myApp.directives", [])


app.directive('appVersion',->
  {
    link : (scope,element,attr)->
      version = jQuery('config').attr('version');
      element.text("website.com V. #{version}");
  }
);

# Rename router for html5
app.directive('a',['$location',($location)->
  {
  restrict  : 'E',
  scope : {},
  link : (scope,element,attr)->
    if $location.$$html5 == false
      rename = (e = null)->
        if e? && (jQuery(e).attr('href')?.match(/[^http|#]/ig))
          href  = jQuery(e).attr('href')
          href  = '/'+href if !href.match(/^\//ig)
          jQuery(e).attr('href','#!'+href);

      # When clink
      element.bind('click',->
        rename(@);
      );
  }
])


# Load session form browser
app.directive('config',['$rootScope','$window','AuthenticationService',($rootScope,$window,AuthenticationService)->
  {
    restrict  : 'E',
    link : (scope,element,attr)->
      AuthenticationService.isLogged  = true if $window.sessionStorage.auth
      AuthenticationService.level     = $window.sessionStorage.level if $window.sessionStorage.level?
  }
])


# Adsense 728
app.directive('ads728', ->
  {
    restrict: 'A',
    templateUrl: 'partials/ads/ads-728.html',
    controller:->
      (adsbygoogle = window.adsbygoogle || []).push({});
  }
);

# Adsense 300
app.directive('ads300', ->
  {
  restrict: 'A',
  templateUrl: 'partials/ads/ads-300.html',
  controller:->
    (adsbygoogle = window.adsbygoogle || []).push({});
  }
);

# Adsense 160
app.directive('ads160', ->
  {
  restrict: 'A',
  templateUrl: 'partials/ads/ads-160.html',
  controller:->
    (adsbygoogle = window.adsbygoogle || []).push({});
  }
);
'use strict';

app = angular.module('myApp.error', [
  'error.routes',
  'error.controllers'
]);
"use strict"

# Controllers
app = angular.module("error.controllers", []);

"use strict"

# Routes
app   = angular.module("error.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider

  .state('/error',{
      url         : '/error',
      templateUrl : 'modules/error/views/index.html'
    })

  .state('/error.page-not-allow',{
      url : '/page-not-allow',
      views : {
        'content' : {
          templateUrl : 'modules/error/views/page-not-allow.html'
        }
      }
    })

]);
"use strict"

# Filters
app   = angular.module("myApp.filters", [])

app.filter("token",()->
  (txt)->
    txt
);
'use strict';

app = angular.module('myApp.home', [
  'home.controllers',
  'home.routes',
]);
"use strict"

# Controllers
app = angular.module("home.controllers", []);

# Home controller
app.controller("HomeCtrl",($scope,$filter,$rootScope)->
  $rootScope.pageTitle      = $filter('translate')('Home title');
);
"use strict"

# Routes
app   = angular.module("home.routes", [])

app.config(['$stateProvider',($stateProvider)->
  $stateProvider
  .state('home', {
      url         : "/home",
      templateUrl : "modules/home/views/index.html",
      controller  : 'HomeCtrl'
    })
]);
'use strict';

app = angular.module('myApp.news', [
  'news.controllers',
  'news.routes',
]);
"use strict"

# Controllers
app = angular.module("news.controllers", []);

app.controller('NewsHomeCtrl',(socket)->

  socket.emit('message','xxx');

  socket.on('sendMessage', (data)->
    console.log(data)
  );
);

"use strict"

# Routes
app   = angular.module("news.routes", [])

app.config(['$stateProvider',($stateProvider)->

  $stateProvider
  .state('news',{
    url         : '/news',
    templateUrl : 'modules/news/views/index.html'
    controller  : 'NewsHomeCtrl'
  })
]);
"use strict"

# Routes
app   = angular.module("myApp.routes", [])

app.config(['$stateProvider','$urlRouterProvider','$locationProvider',($stateProvider, $urlRouterProvider ,$locationProvider)->

  $urlRouterProvider.otherwise("/home");

  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');
]);
"use strict"

# Service
app   = angular.module("myApp.services", ['ngResource']);


app.factory('apiRoot',->
  jQuery('config').attr('base-api');
);


###
  Auth check
###
app.factory "AuthenticationService", ->
  auth  = isLogged  : false;
  level = level     : 'guest';


# Create auth
app.factory('authInterceptor',($rootScope, $q, $window) ->
  return {
    request:(config) ->
      config.headers = config.headers || {};
      if ($window.sessionStorage.auth)
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.auth;
      return config;
    ,
    response: (response) ->
      return response || $q.when(response);
  }
);

# Create token
app.factory('applicationInterceptor',($q) ->
  return {
    request:(config) ->
      config.headers = config.headers || {};
      if angular.isFunction(jQuery)
        config.headers.app_token  = jQuery('config').attr('token');
      else
        throw "Not find jQuery...";
      return config;
    ,
    response: (response) ->
      return response || $q.when(response);
  }
);


###
  Set header before send header
###
app.config(($httpProvider)->
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('applicationInterceptor');
);

app.factory('CategoryApi',($resource,apiRoot)->
  $resource(apiRoot+'/category',{},{
    query : {
      isArray : true
    }
  });

);


###
  Socket
###

app.factory "socket", ($rootScope,apiRoot,$window) ->
  socket = io.connect(apiRoot.replace(/\/v\d+/i,''),{
    'query' : 'token='+ $window.sessionStorage.auth
  })

  on: (eventName, callback) ->
    socket.on eventName, ->
      args = arguments;
      $rootScope.$apply ->
        callback.apply socket, args

  emit: (eventName, data, callback) ->
    socket.emit eventName, data, ->
      args = arguments;
      $rootScope.$apply ->
        callback.apply socket, args  if callback
'use strict';

app = angular.module('myApp.template', [
  'template.controllers',
  'template.directives'
]);
"use strict"

# Controllers
app = angular.module("template.controllers", []);

app.controller('NavTopCtrl',($scope,CategoryApi)->
  CategoryApi.query({}
  ,(data)->
    data        =  _.where(data,{main : true})
    data          =  _.sortBy(data,(val)->
      return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig,'').charCodeAt(0));
    );
    $scope.list   = data;
  ,(err)->

  );
);

"use strict"

# Directives
app   = angular.module("template.directives", []);

# Top menu
app.directive('navTop',->
  {
  scope: {},
  templateUrl: 'modules/template/views/_top.html',
  controller: "NavTopCtrl"
  }
);


# Top menu
app.directive('footer',->
  {
    scope: {},
    templateUrl: 'modules/template/views/_footer.html'
  }
);
"use strict"

# Module
app = angular.module("myApp.translates", ['pascalprecht.translate'])

app.config(['$translateProvider',($translateProvider)->

  $translateProvider.useStaticFilesLoader({
    prefix: '/languages/',
    suffix: '.json'
  });

  $translateProvider.preferredLanguage('th');
]);