(function() {
  'use strict';
  var app;

  app = angular.module('myApp.account', ['account.controllers', 'account.routes', 'account.directives', 'account.services']);

  "use strict";

  app = angular.module("account.controllers", []);


  /*
    For directives (loginForm)
   */

  app.controller('LoginFormCtrl', function($scope, AccountAuthApi, $window, $rootScope, $filter, AuthenticationService, $location) {
    return $scope.loginForm = function(valid) {
      if (valid) {
        return AccountAuthApi.login({
          email: $scope.email,
          password: $scope.password
        }, function(data) {
          AuthenticationService.isLogged = true;
          $window.sessionStorage.level = AuthenticationService.level = data.level;
          $window.sessionStorage.auth = data.auth;
          $scope.email = $scope.password = '';
          $rootScope.alerts.push({
            msg: $filter('translate')('Login already'),
            type: 'success'
          });
          return $location.path("/account");
        }, function(err) {
          AuthenticationService.isLogged = false;
          delete $window.sessionStorage.level;
          delete $window.sessionStorage.auth;
          return $rootScope.alerts.push({
            msg: $filter('translate')('Email or password is incorrect.'),
            type: 'danger'
          });
        });
      } else {
        return $rootScope.alerts.push({
          msg: $filter("translate")("Login please recheck data ?"),
          type: 'danger'
        });
      }
    };
  });


  /*
   Register form
   */

  app.controller('RegisterFormCtrl', function($scope, AccountAuthApi, AccountUsersApi, $window, $rootScope, $filter, AuthenticationService, $location) {
    var autoLogin;
    autoLogin = function() {
      return AccountAuthApi.login({
        email: $scope.email,
        password: $scope.password
      }, function(data) {
        AuthenticationService.isLogged = true;
        $window.sessionStorage.auth = data.auth;
        return $location.path("/account");
      }, function(err) {
        AuthenticationService.isLogged = false;
        delete $window.sessionStorage.level;
        return delete $window.sessionStorage.auth;
      });
    };
    return $scope.registerForm = function(valid) {
      if (valid) {
        return AccountUsersApi.register({
          email: $scope.email,
          password: $scope.password,
          username: $scope.username
        }, function(data) {
          autoLogin();
          $scope.email = $scope.rePassword = $scope.password = '';
          return $rootScope.alerts.push({
            msg: $filter('translate')('Already Registered'),
            type: 'success'
          });
        }, function(err) {
          return $rootScope.alerts.push({
            msg: err.message,
            type: 'warning'
          });
        });
      } else {
        return $rootScope.alerts.push({
          msg: $filter("translate")("Register please recheck data ?"),
          type: 'danger'
        });
      }
    };
  });


  /*
   Account
   */

  app.controller('AccountCtrl', function($scope, $rootScope, $state, $filter, $location, AccountProfileApi, $timeout) {
    $scope.isActive = function(route) {
      var page;
      page = $location.$$path.split('/')[2] || 'home';
      return route === page;
    };
    AccountProfileApi.get({}, function(data) {
      return $scope.data = data;
    });
    if (!$state.params.page) {
      $rootScope.pageTitle = $filter('translate')('Home - Account');
      $scope.saveContact = function(valid) {
        if (valid) {
          return AccountProfileApi.update({
            data: $scope.data
          }, function(data) {
            $rootScope.alerts.push({
              msg: $filter('translate')('Save profile success'),
              type: 'success'
            });
            return $timeout(function() {
              return $rootScope.emptyAlert();
            }, 1000 * 2);
          }, function(err) {
            return $rootScope.alerts.push({
              msg: $filter('translate')('Profile please recheck data?'),
              type: 'danger'
            });
          });
        }
      };
    }
    if ($state.params.page === 'password') {
      $rootScope.pageTitle = $filter('translate')('Password - Account');
      $scope.savePassword = function(valid) {
        if (valid) {
          return AccountProfileApi.password({
            password: $scope.newPassword,
            oldPassword: $scope.oldPassword
          }, function(data) {
            $rootScope.alerts.push({
              msg: $filter('translate')('Save password success'),
              type: 'success'
            });
            return $timeout(function() {
              return $rootScope.emptyAlert();
            }, 1000 * 2);
          }, function(err) {
            return $rootScope.alerts.push({
              msg: $filter('translate')('Old password incorrect'),
              type: 'danger'
            });
          });
        } else {
          return $rootScope.alerts.push({
            msg: $filter('translate')('Password please recheck data'),
            type: 'danger'
          });
        }
      };
    }
    if ($state.params.page === 'favorites') {
      $rootScope.pageTitle = $filter('translate')('Favorites - Account');
    }
    if ($state.params.page === 'history') {
      return $rootScope.pageTitle = $filter('translate')('History - Account');
    }
  });

  "use strict";

  app = angular.module("account.directives", []);


  /*
    Login form
   */

  app.directive('loginForm', function() {
    return {
      scope: {},
      templateUrl: 'modules/account/views/_login.html',
      controller: "LoginFormCtrl"
    };
  });


  /*
    Register form
   */

  app.directive('registerForm', function() {
    return {
      scope: {},
      templateUrl: 'modules/account/views/_register.html',
      controller: "RegisterFormCtrl"
    };
  });


  /*
    Login out and redirect to home
   */

  app.directive('userLogout', [
    '$window', '$location', 'AuthenticationService', '$rootScope', function($window, $location, AuthenticationService, $scope) {
      return {
        link: function(scope, element, attr) {
          return element.bind('click', function() {
            delete $window.sessionStorage.auth;
            delete $window.sessionStorage.level;
            AuthenticationService.isLogged = false;
            AuthenticationService.level = 'guest';
            $location.path("/home");
            return $scope.$apply();
          });
        }
      };
    }
  ]);

  app.directive('emailUnique', [
    'AccountUsersApi', function(AccountUsersApi) {
      return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl) {
          return ele.on('blur', function() {
            return AccountUsersApi.emailUnique({
              email: ele.val()
            }, function(data) {
              if (data.unique === false) {
                ctrl.$setValidity('unique', false);
              }
              if (data.unique === true) {
                return ctrl.$setValidity('unique', true);
              }
            });
          });
        }
      };
    }
  ]);

  app.directive('usernameUnique', [
    'AccountUsersApi', function(AccountUsersApi) {
      return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl) {
          return ele.on('blur', function() {
            return AccountUsersApi.usernameUnique({
              username: ele.val()
            }, function(data) {
              if (data.unique === false) {
                ctrl.$setValidity('unique', false);
              }
              if (data.unique === true) {
                return ctrl.$setValidity('unique', true);
              }
            });
          });
        }
      };
    }
  ]);

  app.directive('equals', function() {
    return {
      restrict: "A",
      require: "?ngModel",
      link: function(scope, elem, attrs, ngModel) {
        var validate;
        if (!ngModel) {
          return;
        }
        scope.$watch(attrs.ngModel, function() {
          validate();
        });
        attrs.$observe("equals", function(val) {
          validate();
        });
        validate = function() {
          var val1, val2;
          val1 = ngModel.$viewValue;
          val2 = attrs.equals;
          ngModel.$setValidity("equals", val1 === val2);
        };
      }
    };
  });

  "use strict";

  app = angular.module("account.routes", []);

  app.config([
    '$stateProvider', function($stateProvider) {
      return $stateProvider.state('register', {
        url: '/register',
        templateUrl: 'modules/account/views/register.html',
        controller: function($rootScope, $filter) {
          return $rootScope.pageTitle = $filter('translate')('Register');
        }
      }).state('login', {
        url: '/login',
        templateUrl: 'modules/account/views/register.html',
        controller: function($scope, $rootScope, $filter) {
          return $rootScope.pageTitle = $filter('translate')('Login');
        }
      }).state('account', {
        url: '/account',
        views: {
          '': {
            templateUrl: 'modules/account/views/index.html'
          },
          'edit@account': {
            templateUrl: 'modules/account/views/account.html',
            controller: 'AccountCtrl'
          }
        },
        access: {
          requiredLogin: true
        }
      }).state('account.edit', {
        url: '/:page',
        views: {
          'edit@account': {
            templateUrl: 'modules/account/views/account.html',
            controller: 'AccountCtrl'
          }
        },
        access: {
          requiredLogin: true
        }
      });
    }
  ]);

  "use strict";

  app = angular.module("account.services", ['ngResource']);

  app.factory("AccountAuthApi", function($resource, apiRoot) {
    return $resource(apiRoot + '/auth', {}, {
      login: {
        url: apiRoot + '/auth',
        method: 'POST',
        params: {}
      }
    });
  });

  app.factory("AccountUsersApi", function($resource, apiRoot) {
    return $resource(apiRoot + '/user', {}, {
      register: {
        method: 'POST'
      },
      emailUnique: {
        url: apiRoot + '/user/email',
        method: 'GET',
        params: {
          email: '@email'
        }
      },
      usernameUnique: {
        url: apiRoot + '/user/username',
        method: 'GET',
        params: {
          username: '@username'
        }
      }
    });
  });


  /*
    Profile api url
   */

  app.factory('AccountProfileApi', function($resource, apiRoot) {
    return $resource(apiRoot + '/profile', {}, {
      get: {
        method: "GET",
        url: apiRoot + '/profile/info',
        cache: false
      },
      update: {
        method: "PUT",
        url: apiRoot + '/profile/info'
      },
      password: {
        method: "PUT",
        url: apiRoot + '/profile/password'
      }
    });
  });

  'use strict';

  app = angular.module('myApp.admin', ['admin.routes', 'admin.controllers', 'admin.directives', 'admin.services', 'admin.categoriesControllers', 'admin.userControllers']);

  "use strict";

  app = angular.module("admin.categoriesControllers", []);

  app.controller('AdminCategoriesCtrl', function($scope, $filter, $rootScope, $state, AdminCategoriesApi, $modal) {
    $rootScope.pageTitle = $filter('translate')('Categories - Admin');
    $scope.page = $state.params.page;
    AdminCategoriesApi.query({}, function(data) {
      if ($state.params.page != null) {
        data = _.where(data, {
          main: false
        });
        data = _.filter(data, function(val) {
          var rex, url;
          url = $state.params.page;
          rex = new RegExp('^\/' + url, 'i');
          return val.url.match(rex);
        });
      } else {
        data = _.where(data, {
          main: true
        });
      }
      data = _.sortBy(data, function(val) {
        return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig, '').charCodeAt(0));
      });
      return $scope.list = data;
    });
    return $scope["delete"] = function(data) {
      if (confirm($filter('translate')('Confirm delete?'))) {
        return AdminCategoriesApi["delete"]({
          id: data._id
        }, function(data) {
          return $state.go('admin.categories', {}, {
            reload: true
          });
        }, function(err) {});
      }
    };
  });

  app.controller('AdminCategoriesSaveCtrl', function($scope, $filter, AdminCategoriesApi, $state, $rootScope) {
    var error, goto;
    if ($state.params.id != null) {
      AdminCategoriesApi.get({
        id: $state.params.id
      }, function(data) {
        return $scope.data = data;
      }, function(err) {});
      $scope.update = true;
    }
    AdminCategoriesApi.query({}, function(data) {
      data = _.where(data, {
        main: true
      });
      data = _.sortBy(data, function(val) {
        return new Date(val.update_at).getTime();
      });
      return $scope.list = data.reverse();
    });
    $scope.selectItem = function(item) {
      var rex, url, _ref, _ref1;
      url = (_ref = $scope.data) != null ? (_ref1 = _ref.url) != null ? _ref1.split('/')[1] : void 0 : void 0;
      rex = new RegExp('^\/' + url, 'i');
      if (item.match(rex)) {
        return true;
      } else {
        return false;
      }
    };
    goto = function(data) {
      var url, _ref;
      if (data.main === true) {
        return $state.go('admin.categories');
      } else {
        url = (_ref = $scope.data.url) != null ? _ref.split('/')[1] : void 0;
        return $state.go('admin.categories.sub', {
          page: url
        });
      }
    };
    error = function() {
      return $rootScope.alerts.push({
        msg: $filter('translate')('Please recheck data?'),
        type: 'danger'
      });
    };
    return $scope.save = function(vaild) {
      if (vaild) {
        if ($state.params.id != null) {
          return AdminCategoriesApi.update({
            data: $scope.data,
            id: $state.params.id
          }, function(data) {
            return goto($scope.data);
          }, function(err) {
            return error();
          });
        } else {
          return AdminCategoriesApi.save({
            data: $scope.data
          }, function(data) {
            return goto($scope.data);
          }, function(err) {
            return error();
          });
        }
      }
    };
  });

  "use strict";

  app = angular.module("admin.controllers", []);

  app.controller('AdminMenuCtrl', function($scope, $location) {
    return $scope.isActive = function(route) {
      return route.split('/')[2] === $location.$$path.split('/')[2];
    };
  });

  app.controller('AdminHomeCtrl', function($scope) {});

  "use strict";

  app = angular.module("admin.directives", []);

  app.directive('adminMenu', function() {
    return {
      templateUrl: 'modules/admin/views/html/_left-menu.html',
      controller: 'AdminMenuCtrl'
    };
  });

  "use strict";

  app = angular.module("admin.routes", []);

  app.config([
    '$stateProvider', function($stateProvider) {
      return $stateProvider.state('admin', {
        url: "/admin",
        templateUrl: "modules/admin/views/home/index.html",
        controller: 'AdminHomeCtrl',
        access: {
          requiredLogin: true,
          level: 'admin'
        }
      }).state('admin.categories', {
        url: "/categories",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/categories/index.html",
            controller: 'AdminCategoriesCtrl'
          }
        },
        access: {
          requiredLogin: true,
          level: 'admin'
        }
      }).state('admin.categories.create', {
        url: "/create",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/categories/_form.html",
            controller: 'AdminCategoriesSaveCtrl'
          }
        },
        access: {
          requiredLogin: true,
          level: 'admin'
        }
      }).state('admin.categories.update', {
        url: "/update/:id",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/categories/_form.html",
            controller: 'AdminCategoriesSaveCtrl'
          }
        },
        access: {
          requiredLogin: true,
          level: 'admin'
        }
      }).state('admin.categories.sub', {
        url: "/sub/:page",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/categories/index.html",
            controller: 'AdminCategoriesCtrl',
            access: {
              requiredLogin: true,
              level: 'admin'
            }
          }
        }
      }).state('admin.user', {
        url: "/user",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/user/index.html",
            controller: "AdminUserCtrl"
          }
        },
        access: {
          requiredLogin: true,
          level: ['admin', 'mod']
        }
      }).state('admin.user.create', {
        url: "/create",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/user/create.html",
            controller: "AdminUserCreateCtrl"
          }
        },
        access: {
          requiredLogin: true,
          level: ['admin', 'mod']
        }
      }).state('admin.user.update', {
        url: "/update/:id",
        views: {
          'content@admin': {
            templateUrl: "modules/admin/views/user/update.html",
            controller: "AdminUserUpdateCtrl"
          }
        },
        access: {
          requiredLogin: true,
          level: ['admin', 'mod']
        }
      });
    }
  ]);

  "use strict";

  app = angular.module("admin.services", ['ngResource']);

  app.factory('AdminCategoriesApi', function($resource, apiRoot) {
    return $resource(apiRoot + '/admin/category/:id', {
      id: '@id'
    }, {
      query: {
        url: apiRoot + '/category',
        method: "GET",
        isArray: true,
        cache: false
      },
      save: {
        method: "POST"
      },
      update: {
        method: "PUT"
      },
      "delete": {
        method: "DELETE"
      }
    });
  });

  app.factory('AdminUserApi', function($resource, apiRoot) {
    return $resource(apiRoot + '/admin/user/:id', {
      id: '@id'
    }, {
      query: {
        url: apiRoot + '/admin/user',
        method: "GET",
        isArray: true
      },
      save: {
        method: "POST"
      },
      update: {
        method: "PUT"
      },
      "delete": {
        method: "DELETE"
      }
    });
  });

  "use strict";

  app = angular.module("admin.userControllers", []);

  app.controller('AdminUserCtrl', function(AdminUserApi, $scope, $window, $filter) {
    AdminUserApi.query({}, function(data) {
      return $scope.list = data;
    }, function(err) {});
    return $scope["delete"] = function(data) {
      if (confirm($filter('translate')('Confirm delete?'))) {
        return AdminUserApi.save({
          id: data._id
        }, function(data) {
          return console.log('yyyy');
        }, function(err) {
          return console.log('nnnn');
        });
      }
    };
  });

  app.controller('AdminUserCreateCtrl', function(AdminUserApi, $scope, $filter) {});

  app.controller('AdminUserUpdateCtrl', function(AdminUserApi, $scope, $filter) {
    return AdminUserApi.update({
      id: data._id
    }, function(data) {
      return $scope.data = data;
    }, function(err) {});
  });

  'use strict';

  app = angular.module('myApp', ['ui.router', 'ui.bootstrap', 'ngSanitize', 'ngAnimate', 'angular-redactor', 'myApp.routes', 'myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers', 'myApp.translates', 'myApp.admin', 'myApp.account', 'myApp.error', 'myApp.news', 'myApp.template', 'myApp.home', 'templates-main']);

  app.run(function($rootScope, $location, AuthenticationService) {
    return $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
      var _ref, _ref1, _ref2;
      if ((((_ref = toState.access) != null ? _ref.level : void 0) != null) && !_.isArray(toState.access.level)) {
        if (toState.access.level !== AuthenticationService.level) {
          $location.path("/error/page-not-allow");
        }
      }
      if ((((_ref1 = toState.access) != null ? _ref1.level : void 0) != null) && _.isArray(toState.access.level)) {
        if (_.indexOf(toState.access.level, AuthenticationService.level) === -1) {
          $location.path("/error/page-not-allow");
        }
      }
      if (((_ref2 = toState.access) != null ? _ref2.requiredLogin : void 0) === true) {
        if (toState.access.requiredLogin !== AuthenticationService.isLogged) {
          $location.path("/login");
        }
      }
      return $rootScope.alerts = [];
    });
  });

  "use strict";

  app = angular.module("myApp.controllers", []);

  app.controller('MainCtrl', function($rootScope, AuthenticationService) {
    $rootScope.pageTitle = '';
    $rootScope.pageDescription = '';
    $rootScope.pageKeyword = '';
    $rootScope.alerts = [];
    $rootScope.closeAlert = function(index) {
      return $rootScope.alerts.splice(index, 1);
    };
    $rootScope.emptyAlert = function() {
      return $rootScope.alerts = [];
    };
    $rootScope.isAdmin = function() {
      return AuthenticationService.level === 'admin';
    };
    $rootScope.isMember = function() {
      return AuthenticationService.level === 'member';
    };
    $rootScope.isGuest = function() {
      return AuthenticationService.level === 'guest';
    };
    return $rootScope.isLogin = function() {
      return AuthenticationService.isLogged;
    };
  });

  "use strict";

  app = angular.module("myApp.directives", []);

  app.directive('appVersion', function() {
    return {
      link: function(scope, element, attr) {
        var version;
        version = jQuery('config').attr('version');
        return element.text("website.com V. " + version);
      }
    };
  });

  app.directive('a', [
    '$location', function($location) {
      return {
        restrict: 'E',
        scope: {},
        link: function(scope, element, attr) {
          var rename;
          if ($location.$$html5 === false) {
            rename = function(e) {
              var href, _ref;
              if (e == null) {
                e = null;
              }
              if ((e != null) && ((_ref = jQuery(e).attr('href')) != null ? _ref.match(/[^http|#]/ig) : void 0)) {
                href = jQuery(e).attr('href');
                if (!href.match(/^\//ig)) {
                  href = '/' + href;
                }
                return jQuery(e).attr('href', '#!' + href);
              }
            };
            return element.bind('click', function() {
              return rename(this);
            });
          }
        }
      };
    }
  ]);

  app.directive('config', [
    '$rootScope', '$window', 'AuthenticationService', function($rootScope, $window, AuthenticationService) {
      return {
        restrict: 'E',
        link: function(scope, element, attr) {
          if ($window.sessionStorage.auth) {
            AuthenticationService.isLogged = true;
          }
          if ($window.sessionStorage.level != null) {
            return AuthenticationService.level = $window.sessionStorage.level;
          }
        }
      };
    }
  ]);

  app.directive('ads728', function() {
    return {
      restrict: 'A',
      templateUrl: 'partials/ads/ads-728.html',
      controller: function() {
        var adsbygoogle;
        return (adsbygoogle = window.adsbygoogle || []).push({});
      }
    };
  });

  app.directive('ads300', function() {
    return {
      restrict: 'A',
      templateUrl: 'partials/ads/ads-300.html',
      controller: function() {
        var adsbygoogle;
        return (adsbygoogle = window.adsbygoogle || []).push({});
      }
    };
  });

  app.directive('ads160', function() {
    return {
      restrict: 'A',
      templateUrl: 'partials/ads/ads-160.html',
      controller: function() {
        var adsbygoogle;
        return (adsbygoogle = window.adsbygoogle || []).push({});
      }
    };
  });

  'use strict';

  app = angular.module('myApp.error', ['error.routes', 'error.controllers']);

  "use strict";

  app = angular.module("error.controllers", []);

  "use strict";

  app = angular.module("error.routes", []);

  app.config([
    '$stateProvider', function($stateProvider) {
      return $stateProvider.state('/error', {
        url: '/error',
        templateUrl: 'modules/error/views/index.html'
      }).state('/error.page-not-allow', {
        url: '/page-not-allow',
        views: {
          'content': {
            templateUrl: 'modules/error/views/page-not-allow.html'
          }
        }
      });
    }
  ]);

  "use strict";

  app = angular.module("myApp.filters", []);

  app.filter("token", function() {
    return function(txt) {
      return txt;
    };
  });

  'use strict';

  app = angular.module('myApp.home', ['home.controllers', 'home.routes']);

  "use strict";

  app = angular.module("home.controllers", []);

  app.controller("HomeCtrl", function($scope, $filter, $rootScope) {
    return $rootScope.pageTitle = $filter('translate')('Home title');
  });

  "use strict";

  app = angular.module("home.routes", []);

  app.config([
    '$stateProvider', function($stateProvider) {
      return $stateProvider.state('home', {
        url: "/home",
        templateUrl: "modules/home/views/index.html",
        controller: 'HomeCtrl'
      });
    }
  ]);

  'use strict';

  app = angular.module('myApp.news', ['news.controllers', 'news.routes']);

  "use strict";

  app = angular.module("news.controllers", []);

  app.controller('NewsHomeCtrl', function(socket) {
    socket.emit('message', 'xxx');
    return socket.on('sendMessage', function(data) {
      return console.log(data);
    });
  });

  "use strict";

  app = angular.module("news.routes", []);

  app.config([
    '$stateProvider', function($stateProvider) {
      return $stateProvider.state('news', {
        url: '/news',
        templateUrl: 'modules/news/views/index.html',
        controller: 'NewsHomeCtrl'
      });
    }
  ]);

  "use strict";

  app = angular.module("myApp.routes", []);

  app.config([
    '$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.otherwise("/home");
      $locationProvider.html5Mode(true);
      return $locationProvider.hashPrefix('!');
    }
  ]);

  "use strict";

  app = angular.module("myApp.services", ['ngResource']);

  app.factory('apiRoot', function() {
    return jQuery('config').attr('base-api');
  });


  /*
    Auth check
   */

  app.factory("AuthenticationService", function() {
    var auth, level;
    auth = {
      isLogged: false
    };
    return level = {
      level: 'guest'
    };
  });

  app.factory('authInterceptor', function($rootScope, $q, $window) {
    return {
      request: function(config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.auth) {
          config.headers.Authorization = 'Bearer ' + $window.sessionStorage.auth;
        }
        return config;
      },
      response: function(response) {
        return response || $q.when(response);
      }
    };
  });

  app.factory('applicationInterceptor', function($q) {
    return {
      request: function(config) {
        config.headers = config.headers || {};
        if (angular.isFunction(jQuery)) {
          config.headers.app_token = jQuery('config').attr('token');
        } else {
          throw "Not find jQuery...";
        }
        return config;
      },
      response: function(response) {
        return response || $q.when(response);
      }
    };
  });


  /*
    Set header before send header
   */

  app.config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
    return $httpProvider.interceptors.push('applicationInterceptor');
  });

  app.factory('CategoryApi', function($resource, apiRoot) {
    return $resource(apiRoot + '/category', {}, {
      query: {
        isArray: true
      }
    });
  });


  /*
    Socket
   */

  app.factory("socket", function($rootScope, apiRoot, $window) {
    var socket;
    socket = io.connect(apiRoot.replace(/\/v\d+/i, ''), {
      'query': 'token=' + $window.sessionStorage.auth
    });
    return {
      on: function(eventName, callback) {
        return socket.on(eventName, function() {
          var args;
          args = arguments;
          return $rootScope.$apply(function() {
            return callback.apply(socket, args);
          });
        });
      },
      emit: function(eventName, data, callback) {
        return socket.emit(eventName, data, function() {
          var args;
          args = arguments;
          return $rootScope.$apply(function() {
            if (callback) {
              return callback.apply(socket, args);
            }
          });
        });
      }
    };
  });

  'use strict';

  app = angular.module('myApp.template', ['template.controllers', 'template.directives']);

  "use strict";

  app = angular.module("template.controllers", []);

  app.controller('NavTopCtrl', function($scope, CategoryApi) {
    return CategoryApi.query({}, function(data) {
      data = _.where(data, {
        main: true
      });
      data = _.sortBy(data, function(val) {
        return parseInt(val.name.replace(/([\u0E2F-\u0E5B a-z])/ig, '').charCodeAt(0));
      });
      return $scope.list = data;
    }, function(err) {});
  });

  "use strict";

  app = angular.module("template.directives", []);

  app.directive('navTop', function() {
    return {
      scope: {},
      templateUrl: 'modules/template/views/_top.html',
      controller: "NavTopCtrl"
    };
  });

  app.directive('footer', function() {
    return {
      scope: {},
      templateUrl: 'modules/template/views/_footer.html'
    };
  });

  "use strict";

  app = angular.module("myApp.translates", ['pascalprecht.translate']);

  app.config([
    '$translateProvider', function($translateProvider) {
      $translateProvider.useStaticFilesLoader({
        prefix: '/languages/',
        suffix: '.json'
      });
      return $translateProvider.preferredLanguage('th');
    }
  ]);

}).call(this);

//# sourceMappingURL=index.js.map
