(function() {
  var app, config, ejs, express, pkg, port;

  express = require('express');

  ejs = require('ejs');

  app = express();

  port = process.env.PORT || 7000;

  app.use(express["static"](__dirname + '/public'));

  app.engine('.html', require('ejs').__express);

  app.set('view engine', 'html');

  app.set('view cache', false);

  app.set('views', __dirname + '/views');

  app.use(require('express-html-snapshots').middleware);

  config = require('./config/env.json')[process.env.NODE_ENV || 'development'];

  pkg = require('./package.json');

  config.version = pkg.version;

  app.route('*').all(function(req, res) {
    return res.render('index.html', {
      config: config
    });
  });

  app.listen(port, function() {
    return console.log('Server run port: ' + port);
  });

}).call(this);
