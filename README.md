Price
======================
Show and compare price all product in thailand

## Live Example
Example on [http://basic-node.herokuapp.com](http://basic-node.herokuapp.com).


## Log

### V 0.2.1 2014-06-23

    * Add socket.io

### V 0.2.0 2014-05-28

    * Add css animate
    * Add javascript underscore
    * Fixed auth
    * Check access when change state
    * Add route admin
    * Add route account
    * Change value baseApiUrl --> apiRoot

### V 0.1.2 2014-05-22

    * Fixed route language
    * Fixed auth re constructor
    * Not use node cookie (token)
    * Fixed move controller in directive --> controller
    * Add directive uniquer email and username
    * Add directive google adsense and template

### V 0.1.1 2014-05-20

    * Add directive login
    * Add directive register
    * Add directive CURD account
    * Add bootstrap template
    * Add angular translate