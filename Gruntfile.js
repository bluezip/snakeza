// Gruntfile.js

// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {

    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({

        // get the configuration info from package.json ----------------------------
        // this way we can use things like name and version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded',
                    debugInfo : true
                },
                files: {
                    './public/css/style.css': './public/css/scss/index.scss'
                }
            }
        },
        less: {
            development: {
                files: {
                    "./public/css/bootstrap.css": "./public/css/less/bootstrap.less"
                }
            }
        },
        coffee: {
            compile: {
                files: {
                    'public/js/index.js': ['public/modules/**/*.coffee'] // compile and concat into single file
                },
                options: {
                    sourceMap: true
                }
            },
            server: {
                files: {
                    'server.js' : ['server.coffee'] // compile server.coffee
                }
            }
        },
        i18nextract: {
            default_options: {
                src     : [ './public/**/*.js', './public/**/*.html'],
                lang    : ['th'],
                dest    : './public/languages'
            }
        },
        html2js: {
            options: {
                base : './public/'
            },
            main: {
                src: ['./public/modules/**/*.html'],
                dest: './public/js/templates.js'
            }
        },
        watch: {
            coffee: {
                files: [
                    './public/modules/**/*.coffee',
                    './server.coffee',
                    './public/modules/account/*.coffee',
                    './public/modules/admin/*.coffee'
                ],
                tasks: ['coffee']
            },
            sass: {
                files: ['./public/css/scss/**/*.scss', './public/modules/**/*.scss'],
                tasks: ['sass']
            },
            'html2js': {
                files: ['./public/modules/**/*.html'],
                tasks: ['html2js']
            }
        }
    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-angular-translate');
    grunt.loadNpmTasks('grunt-html2js');
};
