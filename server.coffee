express       = require('express');
ejs           = require('ejs');


app           = express();
port          = process.env.PORT || 7000;

# Set static page
app.use(express.static(__dirname + '/public'))
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');
app.set('view cache', false);
app.set('views', __dirname + '/views');

# SEO
app.use(require('express-html-snapshots').middleware);


config          = require('./config/env.json')[process.env.NODE_ENV || 'development'];
pkg             = require('./package.json');

config.version  = pkg.version;

#  All route
app.route('*')
  .all((req, res) ->
    res.render('index.html',{ config : config });
  )

app.listen(port,->
  console.log('Server run port: '+port);
);